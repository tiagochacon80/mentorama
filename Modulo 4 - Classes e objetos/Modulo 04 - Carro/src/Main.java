import models.Carros;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Informe a quantidade de pneus: ");
        int quantidadePneus = scanner.nextInt();
        System.out.print("Informe a quantidade de retrovisores: ");
        int quantidadeRetrovisor = scanner.nextInt();
        System.out.print("Informe a quantidade de cintos de segurança do carro: ");
        int cintoSeguranca = scanner.nextInt();
        System.out.print("Informe de parafusos dos pneus: ");
        int quantidadeParafusosPneu = scanner.nextInt();
        System.out.print("Informe o ano de fabricaçao do veiculo: ");
        int anoFabricacao = scanner.nextInt();
        System.out.print("Informe o tipo de combustivel do veiculo: ");
        String combustivel = scanner.next();
        System.out.print("Informe a cor do veiculo: ");
        String cor = scanner.next();
        System.out.print("O veiculo tem ar condicionado (Sim/nao): ");
        String arCondicionado = scanner.next();
        System.out.print("O veiculo possui bancos de couro? (sim/nao): ");
        String bancoCouro = scanner.next();
        System.out.print("Possui suporte para bicicleta? (sim/nao): ");
        String suporteParaBicicleta = scanner.next();

        Carros novoCarro = new Carros(quantidadeRetrovisor, quantidadePneus, quantidadeParafusosPneu, cintoSeguranca);

        novoCarro.setAnoFabricacao(anoFabricacao);
        novoCarro.setCombustivel(combustivel);
        novoCarro.setCor(cor);
        novoCarro.setArCondicionado(arCondicionado);
        novoCarro.setBancoCouro(bancoCouro);
        novoCarro.setSuporteParaBicicleta(suporteParaBicicleta);

        System.out.println("Carro finalizado com sucesso! ");
        System.out.println("Quantidade de pneus: " + quantidadePneus);
        System.out.println("Quantidade de parafusos: " + quantidadeParafusosPneu);
        System.out.println("Quantidade de retrovisores: " + quantidadeRetrovisor);
        System.out.println("O carro possui " + cintoSeguranca + " cintos de segurança");
        System.out.println("Ano de fabricaçao: " + anoFabricacao);
        System.out.println("Tipo de combustivel: " + combustivel);
        System.out.println("Possui banco de couro: " + bancoCouro);
        System.out.println("Cor do veiculo: " + cor);
        System.out.println("Possui ar condicionado: " + arCondicionado);
        System.out.println("Possui banco de couro: " + bancoCouro);
        System.out.println("Possui suporte para bicicleta: " + suporteParaBicicleta);

    }
}
