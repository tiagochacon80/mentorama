package models;

public class Carros {
    //obrigatorios
    private int quantidadeRetrovisor;
    private int quantidadePneus;
    private int cintoSeguranca;
    private int quantidadeParafusosPneu;
    //opcionais
    private int anoFabricacao;
    private String combustivel;
    private String cor;
    private String arCondicionado;
    private String bancoCouro;
    private String suporteParaBicicleta;

    public Carros(int quantidadeRetrovisor, int quantidadePneus, int cintoSeguranca, int quantidadeParafusosPneu) {
            this.quantidadeRetrovisor = quantidadeRetrovisor;
            this.quantidadePneus = quantidadePneus;
            this.cintoSeguranca = cintoSeguranca;
            this.quantidadeParafusosPneu = quantidadePneus * 4;
    }

    public int getQuantidadeRetrovisor() {
        return quantidadeRetrovisor;
    }
    public int getQuantidadePneus() {
         return quantidadePneus;
    }
    public int getCintoSeguranca() {
        return cintoSeguranca;
    }
    public int getQuantidadeParafusosPneu() {
        return quantidadeParafusosPneu;
    }
    public int getAnoFabricacao() {
        return anoFabricacao;
    }
    public void setAnoFabricacao(int anoFabricacao) {
        this.anoFabricacao = this.anoFabricacao;
    }
    public String getCombustivel() {
        return combustivel;
    }
    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }
    public String getCor() {
        return cor;
    }
    public void setCor(String cor) {
        this.cor = cor;
    }
    public String getArCondicionado() {
        return arCondicionado;
    }
    public void setArCondicionado(String arCondicionado) {
        this.arCondicionado = arCondicionado;
    }
    public String getBancoCouro() {
        return bancoCouro;
    }
    public void setBancoCouro(String bancoCouro) {
        this.bancoCouro = bancoCouro;
    }
    public String getSuporteParaBicicleta() {
        return suporteParaBicicleta;
    }
    public void setSuporteParaBicicleta(String suporteParaBicicleta) {
        this.suporteParaBicicleta = suporteParaBicicleta;
    }
}
